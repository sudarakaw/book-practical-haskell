{-# LANGUAGE TemplateHaskell #-}

module Chapter06.KMeans where


import           Data.List
import qualified Data.Map                      as M
import           Lens.Micro.Platform

import           Chapter06.Vector               ( Vector
                                                , Vectorizable
                                                )


data KMeansState e v = KMeansState { _centroids :: [v]
                                   , _points :: [e]
                                   , _err :: Double
                                   , _threshold :: Double
                                   , _steps :: Int
                                   }

makeLenses ''KMeansState


initializeState
  :: (Int -> [e] -> [v]) -> Int -> [e] -> Double -> KMeansState e v
initializeState i k points threshold =
  KMeansState (i k points) points (1.0 / 0.0) threshold 0


-- clusterAssignmentPhase
--   :: (Ord v, Vector v, Vectorizable e v) => [v] -> [e] -> M.Map v [e]
-- clusterAssignmentPhase centeroids points =
--   let initialMap = M.fromList $ zip centeroids (repeat [])
--   in  foldr
--         (\p m ->
--           let chosenC = minimumBy (compareDistance p) centeroids
--           in  M.adjust (p :) chosenC m
--         )
--         initialMap
--         points
--  where
--   compareDistance p x y =
--     compare (distance x $ toVector p) (distance y $ toVector p)


clusterAssignmentPhase
  :: (Vector v, Vectorizable e v) => KMeansState v e -> M.Map v [e]
clusterAssignmentPhase = undefined


-- newCenteroidPhase :: (Vector v, Vectorizable e v) => M.Map v [e] -> [(v, v)]
-- newCenteroidPhase = M.toList . fmap (centeroid . map toVector)


-- shouldStop :: Vector v => [(v, v)] -> Double -> Bool
-- shouldStop centeroids threshold =
--   foldr (\(x, y) s -> s + distance x y) 0.0 centeroids < threshold


kMeans
  :: (Vector v, Vectorizable e v)
  => (Int -> [e] -> [v])  -- initialization function
  -> Int                  -- number of centeroids
  -> [e]                  -- the Information
  -> Double               -- threshold
  -> [v]                  -- final centeroids & number of iterations
kMeans i k points threshold =
  view centroids $ kMeans' (initializeState i k points threshold)

kMeans' :: (Vector v, Vectorizable e v) => KMeansState e v -> KMeansState e v
kMeans' state = undefined
  -- let assignments = clusterAssignmentPhase state
  --     state1 =
  --         state
  --           &  centroids
  --           .  traversed
  --           %~ (\c ->
  --                centeroid $ fmap toVector $ M.findWithDefault [] c assignments
  --              )
  --     state2 = state1 & err .~ sum
  --       (zipWith distance (state ^. centroids) (state1 ^. centroids))
  --     state3 = state2 & steps +~ 1
  -- in  if state3 ^. err < state3 ^. threshold then state3 else kMeans' state3


initializeSample :: Int -> [e] -> [(Double, Double)]
initializeSample 0 _ = []
initializeSample n v =
  (fromIntegral n, fromIntegral n) : initializeSample (n - 1) v


-- TEST DATA

testInfo :: [(Double, Double)]
testInfo = [(1, 1), (1, 2), (4, 4), (4, 5)]
