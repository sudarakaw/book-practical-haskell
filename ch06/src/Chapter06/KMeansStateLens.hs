{-# LANGUAGE TemplateHaskell #-}

module Chapter06.KMeansStateLens where


import           Control.Monad.State
import           Data.List
import qualified Data.Map                      as M
import           Lens.Micro.Platform

import           Chapter06.Vector


data KMeansState v = KMeansState { _centroids :: [v]
                                 , _threshold :: Double
                                 ,_steps :: Int
                                 }

makeLenses ''KMeansState


kMeans' :: (Vector v, Vectorizable e v) => [e] -> State (KMeansState v) [v]
kMeans' points = do
  prevCentrs <- use centroids
  let assignments = clusterAssignments prevCentrs points
      newCentrs   = newCentroids assignments

  centroids .= newCentrs
  steps += 1

  let err = sum $ zipWith distance prevCentrs newCentrs

  t <- use threshold

  if err < t then return newCentrs else kMeans' points


newCentroids :: (Vector v, Vectorizable e v) => M.Map v [e] -> [v]
newCentroids = M.elems . fmap (centroid . map toVector)


clusterAssignments :: (Vector v, Vectorizable e v) => [v] -> [e] -> M.Map v [e]
clusterAssignments centrs points =
  let initialMap = M.fromList $ zip centrs (repeat [])
  in  foldr
        (\p m ->
          let chosenC = minimumBy (compareDistance p) centrs
          in  M.adjust (p :) chosenC m
        )
        initialMap
        points
 where
  compareDistance p x y =
    compare (distance x $ toVector p) (distance y $ toVector p)
