{-# LANGUAGE TemplateHaskell #-}

module Chapter06.Lenses where


import           Lens.Micro.Platform

import           Chapter06.Client


makeLenses ''Client
makeLenses ''Person


fullName :: Lens' Person String
fullName = lens
  (\(Person f l) -> f ++ " " ++ l)
  (\_ fn -> case words fn of
    f : l : _ -> Person f l
    _         -> error "Incorrect name"
  )
