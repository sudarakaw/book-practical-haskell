{-# LANGUAGE LambdaCase #-}

module Chapter06.HandWrittenLenses where


import           Lens.Micro.Platform


data Client i
  = GovOrg i String
  | Company i String Person String
  | Individual i Person
  deriving Show


data Person
  = Person String String
  deriving Show


firstName :: Lens' Person String
firstName = lens (\(Person f _) -> f) (\(Person _ l) v -> Person v l)


lastName :: Lens' Person String
lastName = lens (\(Person _ l) -> l) (\(Person f _) v -> Person f v)


indentifier :: Lens (Client i) (Client j) i j
indentifier = lens
  (\case
    (GovOrg i _     ) -> i
    (Company i _ _ _) -> i
    (Individual i _ ) -> i
  )
  (\client id -> case client of
    GovOrg _ n      -> GovOrg id n
    Company _ n p r -> Company id n p r
    Individual _ p  -> Individual id p
  )


fullName :: Lens' Person String
fullName = lens
  (\(Person f l) -> f ++ " " ++ l)
  (\_ fn -> case words fn of
    f : l : _ -> Person f l
    _         -> error "Incorrect name"
  )


-- TEST DATA


listOfClients :: [Client Integer]
listOfClients =
  [ GovOrg 404 "x"
  , Individual 234 (Person "a" "b")
  , Company 123 "XX" (Person "v" "v") "??"
  , Individual 5 (Person "v" "v")
  , Company 445 "ABC" (Person "p" "q") "Director"
  , Individual 874 (Person "y" "x")
  , Individual 563 (Person "Sudaraka" "Wijesinghe")
  ]
