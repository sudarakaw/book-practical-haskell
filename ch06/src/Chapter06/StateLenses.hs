{-# LANGUAGE TemplateHaskell #-}

module Chapter06.StateLenses where


import           Control.Monad.State
import           Data.Char
import           Lens.Micro.Platform

import           Chapter06.Client


data ExampleSt = ExampleSt { _increment :: Integer
                           , _clients :: [Client Integer]
                           }
                           deriving Show

makeLenses ''ExampleSt
makeLenses ''Client
makeLenses ''Person


fullName :: Lens' Person String
fullName = lens
  (\(Person f l) -> f ++ " " ++ l)
  (\_ fn -> case words fn of
    f : l : _ -> Person f l
    _         -> error "Incorrect name"
  )


zoomCl :: State ExampleSt ()
zoomCl = do
  n <- use increment
  zoom (clients . traversed) $ do
    identifier += n
    person . fullName %= map toUpper
