module Chapter06.Monad where


import           Data.Maybe


-- custom combinator
thenDo :: Maybe a -> (a -> Maybe b) -> Maybe b
thenDo Nothing  _ = Nothing
thenDo (Just x) f = f x

fmap' :: (a -> Maybe b) -> Maybe a -> Maybe (Maybe b)
fmap' f x = x `thenDo` (Just . f)


meanPurchase
  :: Integer -- the client identifier
  -> Double -- the mean purchase
meanPurchase clientId =
  let p = purchaseByClientId clientId
  in  foldr (+) 0.0 $ mapMaybe purchaseValue p


-- v1 - unwrap Maybe values with pattern matching
-- purchaseValue :: Integer -> Maybe Double
-- purchaseValue purchaseId = case numberItemsByPurchaseId purchaseId of
--   Nothing -> Nothing
--   Just n  -> case productIdByPurchaseId purchaseId of
--     Nothing   -> Nothing
--     Just prId -> case priceByProductId prId of
--       Nothing    -> Nothing
--       Just price -> Just $ fromInteger n * price

-- v2 - hide Maybe value unwrapping with a combinator
purchaseValue :: Integer -> Maybe Double
purchaseValue purchaseId =
  numberItemsByPurchaseId purchaseId
    `thenDo` (\n ->
               productIdByPurchaseId purchaseId
                 `thenDo` (\prId ->
                            priceByProductId prId
                              `thenDo` (\price -> Just $ fromInteger n * price)
                          )
             )


-- TEST DATA


purchaseByClientId :: Integer -> [Integer]
purchaseByClientId _ = [1, 2, 3]


productIdByPurchaseId :: Integer -> Maybe Integer
productIdByPurchaseId = Just


numberItemsByPurchaseId :: Integer -> Maybe Integer
numberItemsByPurchaseId = Just


priceByProductId :: Integer -> Maybe Double
priceByProductId _ = Just 1.0
