{-# LANGUAGE TemplateHaskell #-}

module Chapter06.TimeMachine where


import           Lens.Micro.Platform


data TimeMachine = TimeMachine
  { _model :: Model
  , _name :: String
  , _biDirectional :: Bool
  , _price :: Double
  }
  deriving Show


data Model = Model
  { modelId :: Int
  , modelName ::String
  }
  deriving Show

makeLenses ''TimeMachine
