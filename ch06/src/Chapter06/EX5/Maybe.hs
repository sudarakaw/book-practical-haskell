module Chapter06.EX5.Maybe where


import           Control.Monad


-- class Monad' m where
--   return :: a -> m a
--   (>>=) :: m a -> (a -> m b) -> m b


data MayB a = Yay a | Nope
  deriving Show

instance Functor MayB where
  fmap _ Nope    = Nope
  fmap f (Yay x) = Yay $ f x

instance Applicative MayB where
  pure  = Yay
  (<*>) = ap

instance Monad MayB where
  -- return = Yay

  (Yay x) >>= f = f x
  Nope    >>= _ = Nope


mapMayB :: (a -> MayB b) -> [a] -> [b]
mapMayB _ []       = []
mapMayB f (x : xs) = case f x of
  Nope    -> mapMayB f xs
  (Yay y) -> y : mapMayB f xs


purchaseValue :: Integer -> MayB Double
purchaseValue purchaseId =
  numberItemsByPurchaseId purchaseId
    >>= (\n ->
          productIdByPurchaseId purchaseId
            >>= priceByProductId
            >>= (\price -> Yay $ fromInteger n * price)
        )


meanPurchase
  :: Integer -- the client identifier
  -> Double -- the mean purchase
meanPurchase clientId =
  let p = purchaseByClientId clientId
  in  foldr (+) 0.0 $ mapMayB purchaseValue p


purchaseValueWithDo :: Integer -> MayB Double
purchaseValueWithDo purchaseId = do
  n         <- numberItemsByPurchaseId purchaseId
  productId <- productIdByPurchaseId purchaseId
  price     <- priceByProductId productId

  return $ fromInteger n * price


meanPurchaseWithDo
  :: Integer -- the client identifier
  -> Double -- the mean purchase
meanPurchaseWithDo clientId =
  let p = purchaseByClientId clientId
  in  foldr (+) 0.0 $ mapMayB purchaseValue p


-- TEST DATA


purchaseByClientId :: Integer -> [Integer]
purchaseByClientId _ = [1, 2, 3]


productIdByPurchaseId :: Integer -> MayB Integer
productIdByPurchaseId = Yay


numberItemsByPurchaseId :: Integer -> MayB Integer
numberItemsByPurchaseId = Yay


priceByProductId :: Integer -> MayB Double
priceByProductId _ = Yay 1.0
