module Chapter06.EX2.TMLens where


import           Lens.Micro.Platform

import           Chapter06.TimeMachine


timeMachineMarkup :: Double -> [TimeMachine] -> [TimeMachine]
timeMachineMarkup pcnt = map $ markup (* (1 + pcnt / 100))
  where markup f tm = tm & price %~ f

-- solution from https://discord.com/channels/736155840951877642/736930850658189384/750585249469169664
-- timeMachineMarkup pcnt lst = lst & traversed . price %~ (* (1 + pcnt / 100))


-- TEST DATA


timeMachineList :: [TimeMachine]
timeMachineList =
  [ TimeMachine (Model 123 "A") "time machine a" False 23.45
  , TimeMachine (Model 123 "A") "time machine b" True  19.99
  , TimeMachine (Model 124 "B") "time machine c" True  14.65
  , TimeMachine (Model 124 "B") "time machine d" False 28.34
  , TimeMachine (Model 125 "C") "time machine e" True  10
  ]
