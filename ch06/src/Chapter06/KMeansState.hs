module Chapter06.KMeansState where


import           Control.Monad
import           Control.Monad.State
import           Data.List
import qualified Data.Map                      as M

import           Chapter06.Vector


-- type State s a = s -> (a, s)


-- thenDo :: State s a -> (a -> State s b) -> State s b
-- thenDo f g s = let (vF, sF) = f s in g vF sF


data KMeansState v = KMeansState { centroids :: [v]
                                 , threshold :: Double
                                 , steps :: Int
                                 }


newCentroids :: (Vector v, Vectorizable e v) => M.Map v [e] -> [v]
newCentroids = M.elems . fmap (centroid . map toVector)


clusterAssignments :: (Vector v, Vectorizable e v) => [v] -> [e] -> M.Map v [e]
clusterAssignments centrs points =
  let initialMap = M.fromList $ zip centrs (repeat [])
  in  foldr
        (\p m ->
          let chosenC = minimumBy (compareDistance p) centrs
          in  M.adjust (p :) chosenC m
        )
        initialMap
        points
 where
  compareDistance p x y =
    compare (distance x $ toVector p) (distance y $ toVector p)


-- v1 - with generic combinator
-- kMeans' :: (Vector v, Vectorizable e v) => [e] -> State (KMeansState v) [v]
-- kMeans' points =
--   (\s -> (centroids s, s))
--     `thenDo` (\prevCentrs ->
--                (\s -> (clusterAssignments prevCentrs points, s))
--                  `thenDo` (\assignments ->
--                             (\s -> (newCentroids assignments, s))
--                               `thenDo` (\newCentrs ->
--                                          (\s ->
--                                              ((), s { centroids = newCentrs })
--                                            )
--                                            `thenDo` (\_ ->
--                                                       (\s ->
--                                                           ( ()
--                                                           , s
--                                                             { steps = steps s + 1
--                                                             }
--                                                           )
--                                                         )
--                                                         `thenDo` (\_ ->
--                                                                    (\s ->
--                                                                        ( threshold
--                                                                          s
--                                                                        , s
--                                                                        )
--                                                                      )
--                                                                      `thenDo` (\t ->
--                                                                                 (\s ->
--                                                                                     ( sum
--                                                                                       $ zipWith
--                                                                                           distance
--                                                                                           prevCentrs
--                                                                                           newCentrs
--                                                                                     , s
--                                                                                     )
--                                                                                   )
--                                                                                   `thenDo` (\err ->
--                                                                                              if err
--                                                                                                 < t
--                                                                                              then
--                                                                                                (\s ->
--                                                                                                  ( newCentrs
--                                                                                                  , s
--                                                                                                  )
--                                                                                                )
--                                                                                              else
--                                                                                                kMeans'
--                                                                                                  points
--                                                                                            )
--                                                                               )
--                                                                  )
--                                                     )
--                                        )
--                           )
--              )


remain :: a -> (s -> (a, s))
remain x s = (x, s)


access :: (s -> a) -> (s -> (a, s))
access f s = (f s, s)


-- modify :: (s -> s) -> (s -> ((), s))
-- modify f s = ((), f s)


-- v2 - with specific combinators
-- kMeans' :: (Vector v, Vectorizable e v) => [e] -> State (KMeansState v) [v]
-- kMeans' points =
--   access centroids
--     `thenDo` (\prevCentrs ->
--                remain (clusterAssignments prevCentrs points)
--                  `thenDo` (\assignments ->
--                             remain (newCentroids assignments)
--                               `thenDo` (\newCentrs ->
--                                          modify
--                                              (\s -> s { centroids = newCentrs })
--                                            `thenDo` (\_ ->
--                                                       modify
--                                                           (\s -> s
--                                                             { steps = steps s
--                                                                         + 1
--                                                             }
--                                                           )
--                                                         `thenDo` (\_ ->
--                                                                    access
--                                                                        threshold
--                                                                      `thenDo` (\t ->
--                                                                                 remain
--                                                                                     (sum
--                                                                                     $ zipWith
--                                                                                         distance
--                                                                                         prevCentrs
--                                                                                         newCentrs
--                                                                                     )
--                                                                                   `thenDo` (\err ->
--                                                                                              if err
--                                                                                                 < t
--                                                                                              then
--                                                                                                (\s ->
--                                                                                                  ( newCentrs
--                                                                                                  , s
--                                                                                                  )
--                                                                                                )
--                                                                                              else
--                                                                                                kMeans'
--                                                                                                  points
--                                                                                            )
--                                                                               )
--                                                                  )
--                                                     )
--                                        )
--                           )
--              )


-- v3 - with do notation
kMeans' :: (Vector v, Vectorizable e v) => [e] -> State (KMeansState v) [v]
kMeans' points = do
  prevCentrs <- gets centroids

  let assignments = clusterAssignments prevCentrs points
      newCentrs   = newCentroids assignments
  modify (\s -> s { centroids = newCentrs })
  modify (\s -> s { steps = steps s + 1 })

  t <- fmap threshold get

  let err = sum $ zipWith distance prevCentrs newCentrs

  if err < t then return newCentrs else kMeans' points


initializeState
  :: (Vector v, Vectorizable e v)
  => (Int -> [e] -> [v])
  -> Int
  -> [e]
  -> Double
  -> KMeansState v
initializeState i k pts t = KMeansState (i k pts) t 0


-- kMeans :: (Vector v, Vectorizable e v) => Int -> [e] -> Double -> [v]
-- kMeans n pts t = evalState (kMeans' pts) (initializeState n t)
