module Chapter07.EX1.BrokenTM
  ( test
  )
where


import           Control.Monad


brokenThreeJumps :: Integer -> [Integer]
brokenThreeJumps year = do
  j1 <- jumps
  j2 <- jumps
  j3 <- jumps

  return $ foldr (+) year [j1, j2, j3]
  where jumps = [-1, 3, 5]


brokenJumps :: Int -> Integer -> [Integer]
brokenJumps n year = do
  map (foldr (+) year) $ replicateM n jumps
  where jumps = [-1, 3, 5]



test :: IO ()
test = do
  print $ brokenThreeJumps 2000
  print $ brokenJumps 3 2000
