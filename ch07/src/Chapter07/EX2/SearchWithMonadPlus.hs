module Chapter07.EX2.SearchWithMonadPlus
  ( test
  )
where


import           Control.Monad


find_ :: (a -> Bool) -> [a] -> Maybe a
find_ f = msum . fmap (\x -> if f x then Just x else Nothing)


test :: IO ()
test = do
  print $ find_ (== 2) []
