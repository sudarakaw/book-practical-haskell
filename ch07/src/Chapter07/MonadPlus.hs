module Chapter07.MonadPlus
  ( test
  )
where


import           Control.Monad


broken1 :: Integer -> [Integer]
broken1 n = [n - 1, n + 1]

broken2 :: Integer -> [Integer]
broken2 n = [1024, n + 2]


test :: IO ()
test = do
  print $ broken1 73
  print $ broken2 73

  print $ broken1 73 `mplus` broken2 73
