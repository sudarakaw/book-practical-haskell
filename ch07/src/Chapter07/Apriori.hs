module Chapter07.Apriori
  ( test
  )
where


import           Control.Monad
import           Data.List                      ( unfoldr )
import           Data.Set                       ( Set )
import qualified Data.Set                      as S

import           Chapter07.Data


newtype FrequantSet = FrequantSet (Set PurchaseInfo)
  deriving (Ord, Eq)


data AssocRule = AssocRule (Set PurchaseInfo) (Set PurchaseInfo)

instance Show AssocRule where
  show (AssocRule a b) = show a ++ " => " ++ show b


setSupport :: [Transaction] -> FrequantSet -> Double
setSupport trans (FrequantSet sElts) =
  let total = length trans
      f (Transaction tElts) = sElts `S.isSubsetOf` tElts
      supp = length (filter f trans)
  in  fromIntegral supp / fromIntegral total

ruleConfidence :: [Transaction] -> AssocRule -> Double
ruleConfidence trans (AssocRule a b) =
  setSupport trans (FrequantSet $ a `S.union` b)
    / setSupport trans (FrequantSet a)


generateL1 :: Double -> [Transaction] -> [FrequantSet]
generateL1 minSupport transactions = noDup $ do
  Transaction t <- transactions
  e             <- S.toList t
  let fs = FrequantSet $ S.singleton e
  guard $ setSupport transactions fs > minSupport
  return fs

noDup :: Ord a => [a] -> [a]
noDup = S.toList . S.fromList


generateNextLk
  :: Double
  -> [Transaction]
  -> (Int, [FrequantSet])
  -> Maybe ([FrequantSet], (Int, [FrequantSet]))
generateNextLk _ _ (_, []) = Nothing
generateNextLk minSupport transactions (k, lk) =
  let lk1 = noDup $ do
        FrequantSet a <- lk
        FrequantSet b <- lk
        guard $ S.size (a `S.intersection` b) == k - 1
        let fs = FrequantSet $ a `S.union` b
        guard $ setSupport transactions fs > minSupport
        return fs
  in  Just (lk1, (k + 1, lk1))


generateAssocRules :: Double -> [Transaction] -> [FrequantSet] -> [AssocRule]
generateAssocRules minConfidence transactions sets = do
  FrequantSet fs <- sets
  subset@(_ : _) <- powerset $ S.toList fs
  let ssubset = S.fromList subset
      rule    = AssocRule ssubset (fs `S.difference` ssubset)

  guard $ ruleConfidence transactions rule > minConfidence

  return rule

powerset :: [a] -> [[a]]
powerset []       = [[]]
powerset (x : xs) = powerset xs ++ map (x :) (powerset xs)


apriori :: Double -> Double -> [Transaction] -> [AssocRule]
apriori minSupport minConfidence transactions =
  generateAssocRules minConfidence transactions $ concat $ unfoldr
    (generateNextLk minSupport transactions)
    (1, generateL1 minSupport transactions)



-- TEST


test :: IO ()
test = do
  -- print $ clientToPurchaseInfo
  --   (Company "1984 Inc." (Person "George" "Orwell" Male) "Director")
  --
  -- print $ clientToPurchaseInfo (GovOrg "NASA")
  --
  -- print $ clientToPurchaseInfo (Individual $ Person "Marty" "McMartyFace" Male)

  print "Ok!"
