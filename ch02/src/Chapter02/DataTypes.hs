{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}

module Chapter02.DataTypes where

import           Data.Char                      ( toUpper )


data Client
  = GovOrg String
  | Company String Integer Person String
  | Individual Person Bool
  deriving Show


data ClientR
  = GovOrgR { clientRName :: String }
  | CompanyR { clientRName :: String
             , companyId :: Integer
             , person :: PersonR
             , duty :: String
             }
  | IndividualR { person :: PersonR }
  deriving Show


data Person = Person String String Gender
  deriving Show


data PersonR = PersonR { firstName :: String
                       , lastName :: String
                       }
  deriving Show


data Gender = Male | Female | Unknown
  deriving Show


data TimeMachine = TimeMachine Model String Bool Float
  deriving Show


data TimeMachineR = TimeMachineR { model :: ModelR
                                 , timeMachineName :: String
                                 , biDirectional :: Bool
                                 , price :: Float
                                 }
  deriving Show


data Model = Model Int String
  deriving Show


data ModelR = ModelR { modelId :: Int
                     , modelName ::String
                     }
  deriving Show


-- clientName client = case client of
--   GovOrg name                     -> name
--   Company name _ _ _              -> name
--   Individual (Person fNm lNm _) _ -> fNm ++ " " ++ lNm

clientName :: Client -> String
clientName (GovOrg name                    ) = name
clientName (Company name _ _ _             ) = name
clientName (Individual (Person fNm lNm _) _) = fNm ++ " " ++ lNm


companyName :: Client -> Maybe String
companyName client = case client of
  Company name _ _ _ -> Just name
  _                  -> Nothing


responsibility :: Client -> String
responsibility client = case client of
  Company _ _ _ r -> r
  _               -> "Unknown"


specialClient :: Client -> Bool
specialClient (clientName -> "Sudaraka Wijesinghe") = True
specialClient (responsibility -> "Director") = True
specialClient _ = False


clientsByGender :: (Int, Int, Int) -> [Client] -> (Int, Int, Int)
clientsByGender acc       []       = acc
clientsByGender (m, f, u) (c : cs) = case c of
  (Individual (Person _ _ Male) _) -> clientsByGender (1 + m, f, u) cs
  (Individual (Person _ _ Female) _) -> clientsByGender (m, 1 + f, u) cs
  _ -> clientsByGender (m, f, 1 + u) cs


withDiscount :: Float -> [TimeMachine] -> [TimeMachine]
withDiscount _    []       = []
withDiscount pcnt (t : ts) = case t of
  (TimeMachine name model dir price) ->
    TimeMachine name model dir (price * (100 - pcnt) / 100)
      : withDiscount pcnt ts


withDiscountR :: Float -> [TimeMachineR] -> [TimeMachineR]
withDiscountR _ [] = []
withDiscountR pcnt (t@(TimeMachineR { price }) : ts) =
  t { price = (price * (100 - pcnt) / 100) } : withDiscountR pcnt ts


-- -- Without NameFieldPuns
-- greet :: ClientR -> String
-- greet IndividualR { person = PersonR { firstName = fn } } = "Hi, " ++ fn
-- greet CompanyR { clientRName = c } = "Hi, " ++ c
-- greet GovOrgR{}                    = "Welcome"

-- -- With NameFieldPuns
-- greet :: ClientR -> String
-- greet IndividualR { person = PersonR { firstName } } = "Hi, " ++ firstName
-- greet CompanyR { clientRName } = "Hi, " ++ clientRName
-- greet GovOrgR{}                = "Welcome"

-- With NameFieldPuns + RecordWildCards
greet :: ClientR -> String
greet IndividualR { person = PersonR {..} } = "Hi, " ++ firstName
greet CompanyR {..}                         = "Hi, " ++ clientRName
greet GovOrgR{}                             = "Welcome"


nameInCapital :: PersonR -> PersonR
nameInCapital p@(PersonR { firstName = initial : rest }) =
  let newName = (toUpper initial) : rest in p { firstName = newName }
nameInCapital p@(PersonR { firstName = "" }) = p


-- TEST DATA

clientList :: [Client]
clientList =
  [ GovOrg "x"
  , Individual (Person "a" "b" Female) False
  , Company "XX" 123 (Person "v" "v" Female) "??"
  , Individual (Person "v" "v" Female) True
  , Company "ABC" 123 (Person "p" "q" Female) "Director"
  , Individual (Person "y" "x" Male)                 False
  , Individual (Person "Sudaraka" "Wijesinghe" Male) False
  ]

clientRList :: [ClientR]
clientRList =
  [ GovOrgR "x"
  , IndividualR (PersonR "a" "b")
  , CompanyR "XX" 123 (PersonR "v" "v") "??"
  , IndividualR (PersonR "v" "v")
  , CompanyR "ABC" 123 (PersonR "p" "q") "Director"
  , IndividualR (PersonR "y" "x")
  , IndividualR (PersonR "Sudaraka" "Wijesinghe")
  ]

timeMachineList :: [TimeMachine]
timeMachineList =
  [ TimeMachine (Model 123 "A") "time machine a" False 23.45
  , TimeMachine (Model 123 "A") "time machine b" True  19.99
  , TimeMachine (Model 123 "B") "time machine c" True  14.65
  , TimeMachine (Model 123 "B") "time machine d" False 28.34
  , TimeMachine (Model 123 "C") "time machine e" True  10
  ]

timeMachineRList :: [TimeMachineR]
timeMachineRList =
  [ TimeMachineR (ModelR 123 "A") "time machine a" False 23.45
  , TimeMachineR (ModelR 123 "A") "time machine b" True  19.99
  , TimeMachineR (ModelR 123 "B") "time machine c" True  14.65
  , TimeMachineR (ModelR 123 "B") "time machine d" False 28.34
  , TimeMachineR (ModelR 123 "C") "time machine e" True  10
  ]
