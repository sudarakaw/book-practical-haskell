module Chapter02.SimpleFunctions where

firstOrEmpty :: [[Char]] -> [Char]
firstOrEmpty lst = if not (null lst) then head lst else "empty"


-- (+++) :: [a] -> [a] -> [a]
-- lstA +++ lstB = if null lstA [> check emptyness <]
--   then lstB -- base case
--   else head lstA : (tail lstA +++ lstB)

(+++) :: [a] -> [a] -> [a]
[]       +++ lstB = lstB
(x : xs) +++ lstB = x : (xs +++ lstB)


reverse2 :: [a] -> [a]
reverse2 lst = if null lst then [] else reverse2 (tail lst) +++ [head lst]


-- maxmin :: Ord a => [a] -> (a, a)
-- maxmin lst = if null (tail lst)
--   then (head lst, head lst)
--   else
--     ( if (head lst) > fst (maxmin (tail lst))
--       then head lst
--       else fst (maxmin (tail lst))
--     , if (head lst) < snd (maxmin (tail lst))
--       then head lst
--       else snd (maxmin (tail lst))
--     )

-- maxmin :: Ord a => [a] -> (a, a)
-- maxmin lst =
--   let h = head lst
--   in  if null (tail lst)
--         then (h, h)
--         else
--           ( if h > fst (maxmin (tail lst)) then h else fst (maxmin (tail lst))
--           , if h < snd (maxmin (tail lst)) then h else snd (maxmin (tail lst))
--           )

-- maxmin :: Ord a => [a] -> (a, a)
-- maxmin lst =
--   let h = head lst
--   in  if null (tail lst)
--         then (h, h)
--         else (if h > t_max then h else t_max, if h < t_min then h else t_min)
--  where
--   t     = maxmin (tail lst)
--   t_max = fst t
--   t_min = snd t

maxmin :: Ord a => [a] -> (a, a)
maxmin [x] = (x, x)
maxmin (x : xs) =
  (if x > xs_max then x else xs_max, if x < xs_min then x else xs_min)
  where (xs_max, xs_min) = maxmin xs


ackermann :: Int -> Int -> Int
ackermann m n | 0 == m              = 1 + n
              | and [0 < m, 0 == n] = ackermann (m - 1) 1
              | and [0 < m, 0 < n]  = ackermann (m - 1) (ackermann m (n - 1))


unzip' :: [(a, b)] -> ([a], [b])
unzip' []            = ([], [])
unzip' ((x, y) : zs) = (x : xs, y : ys) where (xs, ys) = unzip' zs
