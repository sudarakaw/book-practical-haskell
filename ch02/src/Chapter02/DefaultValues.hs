module Chapter02.DefaultValues where


data ConnType = TCP | UDP
  deriving Show


data UseProxy = NoProxy | Proxy String
  deriving Show


data TimeOut = NoTimeOut | TimeOut Integer
  deriving Show


data ConnOptions = ConnOptions { connType :: ConnType
                             , connSpeed :: Integer
                             , connProxy :: UseProxy
                             , connCaching :: Bool
                             , connKeepAlive :: Bool
                             , connTimeOut :: TimeOut
                             }
  deriving Show


data Connection = Connection String ConnOptions
  deriving Show


connDefaults :: ConnOptions
connDefaults = ConnOptions TCP 0 NoProxy False False NoTimeOut


connect :: String -> ConnOptions -> Connection
connect url options = Connection url options
