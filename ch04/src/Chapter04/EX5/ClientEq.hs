module Chapter04.EX5.ClientEq where


data Client i
  = GovOrg { clientId :: i, clientName :: String }
  | Company { clientId :: i, clientName :: String, person :: Person, duty :: String }
  | Individual { clientId :: i, person :: Person }

instance Eq a => Eq (Client a) where
  (GovOrg id1 name1) == (GovOrg id2 name2) = id1 == id2 && name1 == name2
  (Company id1 name1 p1 duty1) == (Company id2 name2 p2 duty2) =
    id1 == id2 && name1 == name2 && p1 == p2 && duty1 == duty2
  (Individual id1 p1) == (Individual id2 p2) = id1 == id2 && p1 == p2
  _                   == _                   = False


data Person = Person { firstName :: String, lastName :: String }


instance Eq Person where
  (Person fn1 ln1) == (Person fn2 ln2) = fn1 == fn2 && ln1 == ln2


-- TEST DATA

listOfClients :: [Client Integer]
listOfClients =
  [ GovOrg 404 "x"
  , Individual 234 (Person "a" "b")
  , Company 123 "XX" (Person "v" "v") "??"
  , Individual 5 (Person "v" "v")
  , Company 445 "ABC" (Person "p" "q") "Director"
  , Individual 874 (Person "y" "x")
  , Individual 563 (Person "Sudaraka" "Wijesinghe")
  ]


test :: (Bool, Bool, Bool, Bool)
test =
  let c1 = head listOfClients
      c2 = head . tail $ listOfClients
      c3 = head listOfClients
      c4 = head . tail . tail . tail $ listOfClients
  in  (c1 == c2, c1 == c3, c2 == c3, c2 == c4)
