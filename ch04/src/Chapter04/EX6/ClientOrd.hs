module Chapter04.EX6.ClientOrd where


import           Data.List                      ( sort )


class Nameable n where
  name :: n -> String


data Client i
  = GovOrg { clientId :: i, clientName :: String }
  | Company { clientId :: i, clientName :: String, person :: Person, duty :: String }
  | Individual { clientId :: i, person :: Person }
  deriving (Eq)

instance Show (Client a) where
  show (GovOrg _ n     ) = " GovOrg     : " ++ n ++ "\n"
  show (Company _ n _ d) = " Company    : " ++ n ++ " (" ++ d ++ ")\n"
  show (Individual _ p ) = " Individual : " ++ (show p) ++ "\n"

instance Nameable (Client a) where
  name (Individual _ p) = name p
  name c                = clientName c

instance  Eq a =>Ord (Client a) where
  compare c1 c2 =
    let n1 = name c1
        n2 = name c2
    in  if n1 == n2 then compareType c1 c2 else compare n1 n2
   where
    compareType (Individual{}) _          = LT
    compareType (Company{}   ) (GovOrg{}) = LT
    compareType _              _          = GT


data Person = Person { firstName :: String, lastName :: String }
  deriving (Eq, Ord)

instance Show Person where
  show (Person fn ln) = fn ++ " " ++ ln

instance Nameable Person where
  name (Person fn ln) = fn ++ " " ++ ln


-- TEST DATA

listOfClients :: [Client Integer]
listOfClients =
  [ GovOrg 404 "NSA"
  , GovOrg 1   "v v"
  , GovOrg 2   "NASA"
  , Individual 234 (Person "a" "b")
  , Company 123 "XX" (Person "v" "v") "??"
  , Individual 5 (Person "v" "v")
  , Company 445 "ABC" (Person "p" "q") "Director"
  , Individual 874 (Person "y" "x")
  , Individual 563 (Person "Sudaraka" "Wijesinghe")
  , Company 445 "v v" (Person "p" "q") "CEO"
  , Company 446 "v v" (Person "p" "q") "CTO"
  , Company 654 "a b" (Person "p" "q") "CEO"
  , Individual 666 (Person "a" "b")
  ]


test :: [Client Integer]
test = sort listOfClients
