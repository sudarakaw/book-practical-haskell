module Chapter04.EX9.Foldable where


data MayB a
  = Yay a
  | Nope
  deriving Show

instance Foldable MayB where
  foldr f i (Yay x) = f x i
  foldr _ i Nope    = i


data BinaryTree a = Node a (BinaryTree a) (BinaryTree a)
                  | Leaf
                  deriving Show

instance Foldable BinaryTree where
  foldr f i (Node x lt rt) = f x $ foldr f (foldr f i rt) lt
  foldr _ i Leaf           = i
