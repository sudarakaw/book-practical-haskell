module Chapter04.Client
  ( Client(..)
  , Person(..)
  , listOfClients
  , randomClients
  )
where


import           Data.List                      ( unfoldr )
import           System.Random


data Client i
  = GovOrg { clientId :: i, clientName :: String }
  | Company { clientId :: i, clientName :: String, person :: Person, duty :: String }
  | Individual { clientId :: i, person :: Person }
  deriving (Eq, Ord, Show)


data Person = Person { firstName :: String, lastName :: String }
  deriving (Eq, Ord, Read, Show)


-- TEST DATA

listOfClients :: [Client Integer]
listOfClients =
  [ GovOrg 404 "x"
  , Individual 234 (Person "a" "b")
  , Company 123 "XX" (Person "v" "v") "??"
  , Individual 5 (Person "v" "v")
  , Company 445 "ABC" (Person "p" "q") "Director"
  , Individual 874 (Person "y" "x")
  , Individual 563 (Person "Sudaraka" "Wijesinghe")
  ]


{- Random client generation
    By Kartik from Haskell.amsterdam study group
    https://discordapp.com/channels/736155840951877642/736930814255824937/739668216296505356
-}

randomClients :: Int -> Int -> [Client Integer]
randomClients count seed =
  zipWith assignId (unfoldr (Just . client) (mkStdGen seed)) [1 .. count]

client :: RandomGen g => g -> (Client Integer, g)
client g = case randomR (0 :: Int, 2) g of
  (0, g') -> (defaultGovOrg, g')
  (1, g') -> (defaultCompany, g')
  (_, g') -> (defaultIndividual, g')

assignId :: Client Integer -> Int -> Client Integer
assignId c i = c { clientId = toInteger i }

defaultGovOrg :: Client Integer
defaultGovOrg = GovOrg 0 "govorg"

defaultCompany :: Client Integer
defaultCompany = Company 0 "company" defaultPerson "duty"

defaultIndividual :: Client Integer
defaultIndividual = Individual 0 defaultPerson

defaultPerson :: Person
defaultPerson = Person "fn" "ln"
