module Chapter04.SimpleBinaryTrees where


data TravelGuide = TravelGuide { title :: String
                               , authors :: [String]
                               , price :: Double
                               }
                               deriving (Show, Eq)


newtype TGByPrice = TGByPrice TravelGuide
  deriving (Show, Eq)

instance Ord TGByPrice where
  (TGByPrice (TravelGuide t1 a1 p1)) <= (TGByPrice (TravelGuide t2 a2 p2)) =
    p1 < p2 || (p1 == p2 && (t1 < t2 || (t1 == t2 && a1 <= a2)))


data BinaryTree a c = Node a c (BinaryTree a c) (BinaryTree a c)
                    | Leaf deriving
                    (Show, Eq, Ord)


-- treeFind :: Ord a => a -> BinaryTree a -> Maybe a
-- treeFind t (Node v lt rt) = case compare t v of
--   EQ -> Just v
--   LT -> treeFind t lt
--   GT -> treeFind t rt
-- treeFind _ Leaf = Nothing


treeInsert :: (Ord a, Monoid c) => a -> c -> BinaryTree a c -> BinaryTree a c
treeInsert v c (Node v2 c2 lt rt) = case compare v v2 of
  EQ -> Node v2 c2 lt rt
  LT ->
    let newLt = treeInsert v c lt
        newC  = c2 <> cached newLt <> cached rt
    in  Node v2 newC newLt rt
  GT ->
    let newRt = treeInsert v c rt
        newC  = c2 <> cached lt <> cached newRt
    in  Node v2 newC lt newRt
treeInsert v c Leaf = Node v c Leaf Leaf


cached :: Monoid c => BinaryTree v c -> c
cached (Node _ c _ _) = c
cached Leaf           = mempty


-- TEST DATA

tg1 :: TravelGuide
tg1 = TravelGuide "TG 1" ["Author A", "Author B"] 12.34

tg2 :: TravelGuide
tg2 = TravelGuide "TG 2" ["Author B", "Author C"] 23.45

tg3 :: TravelGuide
tg3 = TravelGuide "TG 3" ["Author D"] 1.23

tg4 :: TravelGuide
tg4 = TravelGuide "TG 4" ["Author C"] 3.56

tg5 :: TravelGuide
tg5 = TravelGuide "TG 5" ["Author B"] 2.87
