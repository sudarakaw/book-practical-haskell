module Chapter04.EX4.Priceable where


class Priceable p where
  price :: p -> Double


data TimeMachine = TimeMachine
  { model :: Model
  , timeMachineName :: String
  , biDirectional :: Bool
  , timeMachinePrice :: Double
  }

instance Priceable TimeMachine where
  price = timeMachinePrice


data Model = Model
  { modelId :: Int
  , modelName ::String
  }


data Guide = Guide
  { title :: String
  , guidePrice :: Double
  }

instance Priceable Guide where
  price = guidePrice


data Tool = Tool
  { description :: String
  , toolPrice :: Double
  }

instance Priceable Tool where
  price = toolPrice


totalPrice :: Priceable p => [p] -> Double
totalPrice = foldr ((+) . price) 0


-- TEST DATA


timeMachineList :: [TimeMachine]
timeMachineList =
  [ TimeMachine (Model 123 "A") "time machine a" False 23.45
  , TimeMachine (Model 123 "A") "time machine b" True  19.99
  , TimeMachine (Model 123 "B") "time machine c" True  14.65
  , TimeMachine (Model 123 "B") "time machine d" False 28.34
  , TimeMachine (Model 123 "C") "time machine e" True  10
  ]


guideList :: [Guide]
guideList =
  [Guide "The Hitchhiker's Guide" 34.24, Guide "Time Machine for Dummies" 23.98]


toolList :: [Tool]
toolList = [Tool "Flux Capacitor" 345.99, Tool "Sonic Screw Driver" 234.56]
