module Chapter04.EX2.AlterMap where


import qualified Data.Map                      as M
import           Data.Map                       ( Map )


insert :: Ord k => k -> a -> Map k a -> Map k a
insert key value = M.alter (\_ -> Just value) key


delete :: Ord k => k -> Map k a -> Map k a
delete = M.alter (\_ -> Nothing)


adjust :: Ord k => (a -> a) -> k -> Map k a -> Map k a
adjust f = M.alter g
 where
  g (Just x) = Just $ f x
  g _        = Nothing


-- TEST DATA

m0 :: Map String Integer
m0 = M.empty

m1 :: Map String Integer
m1 = M.singleton "a" 1

m2 :: Map String Integer
m2 = M.insert "b" 2 m1

m3 :: Map String Integer
m3 = M.insert "a" 3 m2

m4 :: Map String Integer
m4 = M.insertWith (+) "a" 4 m3
