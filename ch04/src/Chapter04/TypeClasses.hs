module Chapter04.TypeClasses where


import           Chapter04.Client

class Nameable n where
  name :: n -> String


initial :: Nameable a => a -> Char
initial = head . name


instance Nameable Person where
  name Person { firstName = fn, lastName = ln } = fn ++ " " ++ ln


instance Nameable (Client i) where
  name (Individual _ p) = name p
  name c                = clientName c
