module Chapter04.EX3.Classifying where


import qualified Data.Set                      as S
import           Data.Set                       ( Set )
import qualified Data.Map                      as M
import           Data.Map                       ( Map )

import           Chapter04.Client               ( Client(..)
                                                , randomClients
                                                )


data ClientKind
  = GovOrgKind
  | CompanyKind
  | IndividualKind
  deriving (Eq, Ord, Show)


kindOf :: Client a -> ClientKind
kindOf (GovOrg{}    ) = GovOrgKind
kindOf (Company{}   ) = CompanyKind
kindOf (Individual{}) = IndividualKind


classifyClientsA :: [Client Integer] -> Map ClientKind (Set (Client Integer))
classifyClientsA = foldr addToMap M.empty
 where
  addToMap c = M.alter (addToSet c) (kindOf c)

  addToSet c Nothing  = Just $ S.singleton c
  addToSet c (Just s) = Just $ S.insert c s


classifyClientsB :: [Client Integer] -> Map ClientKind (Set (Client Integer))
classifyClientsB clients =
  M.fromListWith S.union $ map (\c -> (kindOf c, S.singleton c)) clients


-- run in GHCi with `:set +s`
testClassification
  :: Int
  -> Int
  -> ([Client Integer] -> Map ClientKind (Set (Client Integer)))
  -> IO ()
testClassification seed count f =
  writeFile "/dev/null" . show . fmap S.size . f $ randomClients count seed
