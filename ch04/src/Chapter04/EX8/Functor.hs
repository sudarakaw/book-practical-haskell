module Chapter04.EX8.Functor where


data MayB a
  = Yay a
  | Nope
  deriving Show

instance Functor MayB where
  fmap f (Yay x) = Yay $ f x
  fmap _ Nope    = Nope


data BinaryTree a = Node a (BinaryTree a) (BinaryTree a)
                  | Leaf
                  deriving Show

instance Functor BinaryTree where
  fmap f (Node x lt rt) = Node (f x) (fmap f lt) (fmap f rt)
  fmap _ Leaf           = Leaf
