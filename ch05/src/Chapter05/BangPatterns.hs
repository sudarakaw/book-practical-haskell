{-# LANGUAGE BangPatterns #-}

module Chapter05.BangPatterns where


import           Chapter05.TimeMachine


sumYears :: [TimeMachine] -> Integer
sumYears = sumYears' 0
 where
  sumYears' x []             = x
  sumYears' x (TM _ !y : ys) = let !sum = x + y in sumYears' sum ys
