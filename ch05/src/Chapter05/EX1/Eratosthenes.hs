module Chapter05.EX1.Eratosthenes where


primes :: [Integer]
primes =
  map head $ iterate (\(x : xs) -> filter (\y -> y `mod` x /= 0) xs) [2 ..]
