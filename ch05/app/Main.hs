module Main where


import           Data.List                      ( foldl' )


main :: IO ()
main = putStrLn $ show result


result :: Integer
-- result = foldr (*) 1 [1 .. 100000]
-- result = foldl (*) 1 [1 .. 100000]
result = foldl' (*) 1 [1 .. 100000]
