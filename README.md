# Practical Haskell - Code Samples

Practice code written while following Haskell Amsterdam study group.

- Book [Practical Haskell - A Real World Guide to Programming](https://www.apress.com/gp/book/9781484244791) by [Alejandro Serrano](https://twitter.com/trupill)
- Study group organized by [Haskell.amsterdam](https://www.haskell.amsterdam/).
