module Chapter03.Lists2 where


import           Data.Function
import           Data.List

import           Chapter03.ParamPoly


compareClient :: Client a -> Client a -> Ordering
compareClient (Individual { person = p1 }) (Individual { person = p2 }) =
  compare (firstName p1) (firstName p2)
compareClient (Individual{}) _ = GT
compareClient _ (Individual{}) = LT
compareClient c1 c2 = compare (clientName c1) (clientName c2)


-- companyDutiesAnalytics :: [Client a] -> [String]
-- companyDutiesAnalytics =
--   map (duty . head)
--     . sortBy (\x y -> compare (length y) (length x))
--     . groupBy (\x y -> duty x == duty y)
--     . filter isCompany
--  where
--   isCompany (Company{}) = True
--   isCompany _           = False

companyDutiesAnalytics :: [Client a] -> [String]
companyDutiesAnalytics =
  map (duty . head)
    . sortBy (flip (compare `on` length))
    . groupBy ((==) `on` duty)
    . filter isCompany
 where
  isCompany (Company{}) = True
  isCompany _           = False


-- TEST DATA


listOfClients :: [Client Integer]
listOfClients =
  [ Individual 2 (Person "H. G." "Wells")
  , GovOrg 3 "NTTF" -- National Time Travel Foundation
  , Company 4 "Wormhole Inc." (Person "Karl" "Schwarzschild") "Physicist"
  , Individual 5 (Person "Doctor" "")
  , Individual 6 (Person "Sarah" "Jane")
  ]
