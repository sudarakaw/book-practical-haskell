{-# LANGUAGE LambdaCase #-}

module Chapter03.FnsParams where


apply3f2 :: (Integer -> Integer) -> Integer -> Integer
apply3f2 f x = 3 * f (x + 2)


equalTuples :: [(Integer, Integer)] -> [Bool]
equalTuples t = map (\(x, y) -> x == y) t


-- -- Without LambdaCase
-- sayHello :: [String] -> [String]
-- sayHello names = map
--   (\name -> case name of
--     "Suda" -> "Hello, learner"
--     _      -> "Welcome, " ++ name
--   )
--   names

-- With LambdaCase
sayHello :: [String] -> [String]
sayHello names = map
  (\case
    "Suda" -> "Hello, learner"
    name   -> "Welcome, " ++ name
  )
  names


multiplyByN :: Integer -> (Integer -> Integer)
multiplyByN n = \x -> n * x


filterOnes :: [Integer] -> [Integer]
filterOnes lst = filter (\x -> 1 == x) lst


filterANumber :: Integer -> [Integer] -> [Integer]
filterANumber n lst = filter (\x -> n == x) lst


filterNot :: (a -> Bool) -> [a] -> [a]
filterNot f lst = filter (\x -> not (f x)) lst
