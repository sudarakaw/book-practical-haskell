{-# LANGUAGE LambdaCase #-}

module Chapter03.ParamPoly where

maybeString :: Maybe a -> [Char]
maybeString (Just _) = "Just"
maybeString Nothing  = "Nothing"


data Client i
  = GovOrg { clientId :: i, clientName :: String }
  | Company { clientId :: i, clientName :: String, person :: Person, duty :: String }
  | Individual { clientId :: i, person :: Person }
  deriving Show


data Person = Person { firstName :: String, lastName :: String }
  deriving Show


swapTriple :: (a, b, c) -> (b, c, a)
swapTriple (x, y, z) = (y, z, x)


duplicate :: a -> (a, a)
duplicate x = (x, x)


nothing :: a -> Maybe b
nothing _ = Nothing



-- index :: Num b => [a] -> [(b, a)]
index :: [a] -> [(Integer, a)]
index []       = []
index [x     ] = [(0, x)]
index (x : xs) = let indexed@((n, _) : _) = index xs in (n + 1, x) : indexed


maybeA :: [a] -> Char
maybeA []      = 'a'
maybeA (_ : _) = 'a'


isGovOrg :: Client a -> Bool
isGovOrg cli = case cli of
  GovOrg _ _ -> True
  _          -> False


safeClientName :: Client a -> String
safeClientName (Individual _ p) = personName p
safeClientName c                = clientName c


personName :: Person -> String
personName (Person fName lName) = fName ++ " " ++ lName


-- -- With auxiliary function
-- filterGovOrgs :: [Client a] -> [Client a]
-- filterGovOrgs cs = filter isGovOrg cs


-- With LambdaCase function
filterGovOrgs :: [Client a] -> [Client a]
filterGovOrgs cs = filter
  (\case
    GovOrg _ _ -> True
    _          -> False
  )
  cs


-- TEST DATA

clientList :: [Client Integer]
clientList =
  [ GovOrg 404 "x"
  , Individual 234 (Person "a" "b")
  , Company 123 "XX" (Person "v" "v") "??"
  , Individual 5 (Person "v" "v")
  , Company 445 "ABC" (Person "p" "q") "Director"
  , Individual 874 (Person "y" "x")
  , Individual 563 (Person "Sudaraka" "Wijesinghe")
  ]
