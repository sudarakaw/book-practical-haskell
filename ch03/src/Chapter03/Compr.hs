{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TransformListComp #-}


module Chapter03.Compr where


import           GHC.Exts

import           Chapter03.ParamPoly


duplicateOdds :: [Integer] -> [Integer]
-- duplicateOdds lst = map (* 2) $ filter odd lst
duplicateOdds lst = [ x * 2 | x <- lst, odd x ]


companyAnalytics :: [Client a] -> [(String, [(Person, String)])]
companyAnalytics cs =
  [ (the clientName, zip person duty)
  | c@(Company {..}) <- cs
  , then sortWith by duty
  , then group by clientName using groupWith
  , then sortWith by length c
  ]
