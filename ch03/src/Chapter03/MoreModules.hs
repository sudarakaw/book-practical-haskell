module Chapter03.MoreModules where


import qualified Data.List                     as L
                                                ( permutations )


permutationsStartingWith :: Char -> String -> [String]
permutationsStartingWith letter =
  filter (\w -> head w == letter) . L.permutations
