{-# LANGUAGE NoImplicitPrelude #-}


module Chapter03.Lists where


import           Prelude                 hiding ( filter
                                                , foldr
                                                , foldl
                                                )


data InfNumber a
  = MinusInfinity
  | Number a
  | PlusInfinity
  deriving Show


filter :: (a -> Bool) -> [a] -> [a]
filter _ []       = []
filter f (x : xs) = if f x then x : filter f xs else filter f xs


foldr :: (a -> b -> b) -> b -> [a] -> b
foldr _ i []       = i
foldr f i (x : xs) = f x (foldr f i xs)


foldl :: (b -> a -> b) -> b -> [a] -> b
foldl _ i []       = i
foldl f i (x : xs) = foldl f (f i x) xs


-- 👍 foldr max 0 [-1,-2,-3] ~> 0
-- 👎 foldr max 0 [1,2,3] ~> 3
--
-- 👍 foldr infMax MinusInfinity $ map Number [-1, -2, -3] ~> Number (-1)
-- 👍 foldr infMax MinusInfinity $ map Number [1, 2, 3] ~> Number 3
--
-- 👍 foldr (\x i -> infMax (Number x) i) MinusInfinity [-1, -2, -3] ~> Number (-1)
-- 👍 foldr (\x i -> infMax (Number x) i) MinusInfinity [1, 2, 3] ~> Number 3
infMax :: Ord a => InfNumber a -> InfNumber a -> InfNumber a
infMax MinusInfinity x             = x
infMax x             MinusInfinity = x
infMax PlusInfinity  _             = PlusInfinity
infMax _             PlusInfinity  = PlusInfinity
infMax (Number a)    (Number b)    = Number $ max a b
