-- Exercise 3-3 solutions WITH Higher-Order Functions.

{-# LANGUAGE NoImplicitPrelude #-}

module Chapter03.EX3.HOF where

import           Prelude                 hiding ( product
                                                , all
                                                )

import           Chapter03.ParamPoly            ( Client
                                                , safeClientName
                                                )


product :: [Integer] -> Integer
product []       = error "empty list"
product (x : xs) = foldr (*) x xs


minimumClient :: [Client a] -> Client a
minimumClient []         = error "No clients"
minimumClient (cl : cls) = foldr compareClients cl cls
 where
  compareClients c1 c2 =
    if (length $ safeClientName c1) > (length $ safeClientName c2)
      then c2
      else c1


all :: [Bool] -> Bool
all []       = error "empty list"
all (b : bs) = foldr (&&) b bs
