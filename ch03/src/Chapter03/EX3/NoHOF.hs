-- Exercise 3-3 solutions WITHOUT Higher-Order Functions.

{-# LANGUAGE NoImplicitPrelude #-}

module Chapter03.EX3.NoHOF where

import           Prelude                 hiding ( product
                                                , all
                                                )

import           Chapter03.ParamPoly            ( Client
                                                , safeClientName
                                                )


product :: [Integer] -> Integer
product []       = error "empty list"
product (x : []) = x
product (x : xs) = x * product xs


minimumClient :: [Client a] -> Client a
minimumClient []       = error "No clients"
minimumClient (c : []) = c
minimumClient (c : cs) =
  let minCs = minimumClient cs
  in  if (length $ safeClientName c) > (length $ safeClientName minCs)
        then minCs
        else c


all :: [Bool] -> Bool
all []       = error "empty list"
all (b : []) = b
all (b : bs) = b && all bs
