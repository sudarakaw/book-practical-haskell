module Chapter03.EX3.MapProof where


mapAsFold :: (a -> b) -> [a] -> [b]
-- mapAsFold f = foldr (\x lst -> f x : lst) []
-- mapAsFold f (y : ys) = foldr (\x lst -> f x : lst) [] (y : ys)
mapAsFold f (y : ys) = f y : foldr (\x lst -> f x : lst) [] ys
