module Chapter03.EX3.Mini where


import           Data.List


elem' :: (Eq a) => a -> [a] -> Bool
elem' x ys = case find ((==) x) ys of
  Nothing -> False
  Just _  -> True
